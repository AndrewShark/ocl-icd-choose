# OCL-ICD-CHOOSE
This utility allows you to specify a set of icd files from `/etc/OpenCL/vendors` that your application will see.

This is useful for testing how applications behave with different set and different order of icd files without the need of (re-/de-)installation of packages providing opencl-driver.

## Usage
`ocl-icd-choose <icdfile.icd[:icdfile2.icd[...]]> <app and its parameters>`

For example:

`$ ocl-icd-choose amdocl64.icd:mesa.icd davinci-resolve-checker`

This will start davinci-resolve-checker which will see two specified icd files in specified order, and no any other icd file.

## Installation

Place the `ocl-icd-choose` in some directory that your `PATH` is aware of.

### Arch Linux

You can install from AUR `ocl-icd-choose`.
  